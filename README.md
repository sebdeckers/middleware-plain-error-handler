# middleware-plain-error-handler

Plain error reporting for Connect and Express with as little magic as possible.

## Why?

Unlike the built-in error handler, this package:
- Does not reveal source code, stack traces, and line numbers.
- Reliably works the same way whether app is in "production" or "development" mode.
- Is careful enough to avoid warnings when using HTTP/2.

## Error Support

Accepts either `HttpError` instances, generic `Error` instances, or any object implementing a `{status, message}` interface.

- `status` is a number `>=400` and `<600`, and
- `message` is a string to show in the HTTP response body.

## Usage

```js
import {errorHandler} from 'middleware-plain-error-handler'
import createError from 'http-errors'
import express from 'express'

const app = express()

app.get('/ddos', (req, res, next) => {
  next(new Error())
})

app.get('/whoami', (req, res, next) => {
  next(createError(418))
})

// Always add the error handling middleware last!
app.use(errorHandler())

// Request /ddos
// -> 500 - Internal Server Error

// Request /whoami
// -> 418 - I'm a teapot
```

## See also

- [@http2/server](https://www.npmjs.com/package/@http2/server) An HTTP/2 server using this package.
- [http-errors](https://www.npmjs.com/package/http-errors) Utility to throw valid HTTP errors.
