const test = require('blue-tape')
const express = require('express')
const request = require('supertest')
const createError = require('http-errors')
const { errorHandler } = require('..')

test('Drop content encoding header', async (t) => {
  const app = express()
    .use((req, res, next) => {
      res.setHeader('content-encoding', 'br')
      next(createError(500))
    })
    .use(errorHandler())
  await request(app)
    .get('/')
    .expect(500)
    .then(({ headers }) => {
      t.is(headers['content-type'], 'text/plain; charset=utf-8')
      t.notOk('content-encoding' in headers)
    })
})
