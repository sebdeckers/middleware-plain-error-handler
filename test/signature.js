const test = require('blue-tape')
const { errorHandler } = require('..')

test('Error middleware signature', async (t) => {
  const middleware = errorHandler()
  t.is(middleware.length, 4)
})
