const test = require('blue-tape')
const express = require('express')
const request = require('supertest')
const createError = require('http-errors')
const { errorHandler } = require('..')

const fixtures = [
  {
    label: 'HTTP error',
    given: (req, res, next) => {
      next(createError(418))
    },
    expected: {
      code: 418,
      message: `I'm a teapot`
    }
  },
  {
    label: 'Plain error',
    given: (req, res, next) => {
      next(new Error())
    },
    expected: {
      code: 500,
      message: 'Internal Server Error'
    }
  },
  {
    label: 'Low 4xx status code exposes the error message',
    given: (req, res, next) => {
      next(createError(451))
    },
    expected: {
      code: 451,
      message: 'Unavailable For Legal Reasons'
    }
  },
  {
    label: 'High 5xx status code hides the error message',
    given: (req, res, next) => {
      next(createError(511))
    },
    expected: {
      code: 511,
      message: 'Error'
    }
  },
  {
    label: 'Custom object',
    given: (req, res, next) => {
      next({
        status: 555,
        message: 'Kaboom!'
      })
    },
    expected: {
      code: 555,
      message: 'Kaboom!'
    }
  },
  {
    label: 'Pile of Poo',
    given: (req, res, next) => {
      next({
        status: 555,
        message: '💩'
      })
    },
    expected: {
      code: 555,
      message: '💩'
    }
  },
  {
    label: 'Invalid error status code',
    given: (req, res, next) => {
      next({
        status: 200,
        message: 'OK'
      })
    },
    expected: {
      code: 500,
      message: 'Internal Server Error'
    }
  },
  {
    label: 'Code without message',
    given: (req, res, next) => {
      next({
        status: 555,
        message: undefined
      })
    },
    expected: {
      code: 555,
      message: 'Error'
    }
  },
  {
    label: 'Headers already sent',
    given: (req, res, next) => {
      const error = createError(416)
      res.writeHead(200, 'OK', {
        'content-type': 'text/plain; charset=utf-8',
        'content-length': error.message.length
      })
      next(error)
    },
    expected: {
      code: 200,
      message: 'Range Not Satisfiable'
    }
  },
  {
    label: 'Body already sent',
    given: (req, res, next) => {
      const error = createError(416)
      res
        .set('content-type', 'text/plain; charset=utf-8')
        .send('OK')
      next(error)
    },
    expected: {
      code: 200,
      message: 'OK'
    }
  },
  {
    label: 'HTTP/1 supports custom status messages',
    given: (req, res, next) => {
      next(createError(500))
    },
    expected: {
      status: 'Internal Server Error',
      code: 500,
      message: 'Error'
    }
  },
  {
    label: 'HTTP/2 does not support status messages',
    given: (req, res, next) => {
      req.httpVersion = '2.0'
      req.httpVersionMajor = 2
      req.httpVersionMinor = 0
      const writeHead = res.writeHead.bind(res)
      res.writeHead = (...args) => {
        if (args.length > 2) {
          throw new Error('Status message passed')
        }
        return writeHead(...args)
      }
      next(createError(500))
    },
    expected: {
      code: 500,
      message: 'Error'
    }
  },
  {
    label: 'HEAD method does not have a body',
    given: (req, res, next) => {
      req.method = 'HEAD'
      next(createError(500))
    },
    expected: {
      status: 'Internal Server Error',
      code: 500,
      message: ''
    }
  }
]

for (const { label, given, expected: { code, message, status } } of fixtures) {
  test(label, async (t) => {
    const app = express()
    app.use(given)
    app.use(errorHandler())
    await request(app)
      .get('/')
      .expect(code, message)
      .expect('content-length', `${Buffer.byteLength(message)}`)
      .expect('content-type', 'text/plain; charset=utf-8')
      .expect((res) => {
        if (status !== undefined) {
          t.is(res.res.statusMessage, status)
        }
      })
      .expect(() => t.pass())
  })
}
