const test = require('blue-tape')
const express = require('express')
const request = require('supertest')
const createError = require('http-errors')
const { errorHandler } = require('..')

test('Continue middleware pipeline', async (t) => {
  let flag = false
  const app = express()
    .use((req, res, next) => next(createError(500)))
    .use(errorHandler())
    .use((req, res, next) => { flag = true })
  await request(app)
    .get('/')
    .expect(500)
    .then((response) => t.is(flag, true))
})
