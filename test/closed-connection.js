const test = require('blue-tape')
const { errorHandler } = require('..')
const { ImATeapot } = require('http-errors')
const http2 = require('http2')
const http1 = require('http')
const connect = require('connect')

function setupServer (t, http, listening) {
  const app = connect()
  app.use((request, response, next) => {
    request.on('aborted', () => {
      next(new ImATeapot())
    })
  })
  app.use(errorHandler())
  app.use((request, response, next) => {
    t.pass('Safely handled closed stream.')
    server.close(t.end)
  })
  app.use((error, request, response, next) => {
    t.fail(error) // Threw error for closed stream.
    server.close(t.end)
  })
  const server = http.createServer(app)
  server.listen(0, () => {
    const { port } = server.address()
    const url = `http://localhost:${port}`
    listening(url)
  })
}

test('Gracefully handle HTTP/2 disconnection', (t) => {
  setupServer(t, http2, (url) => {
    http2.connect(url, (session) => {
      setTimeout(() => {
        const stream = session.request({})
        stream.close(0, () => {
          session.close()
        })
      }, 200)
    })
  })
})

test('Gracefully handle HTTP/1 disconnection', (t) => {
  setupServer(t, http1, (url) => {
    const request = http1.request(url)
    request.end(() => {
      request.socket.destroy()
    })
    request.on('error', () => {})
  })
})
