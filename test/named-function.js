const test = require('blue-tape')
const { errorHandler } = require('..')

test('Named middleware function', async (t) => {
  const middleware = errorHandler()
  t.is(middleware.name, 'errorHandler')
})
