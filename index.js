const statuses = require('statuses')

module.exports.errorHandler =
() => function errorHandler (error, request, response, next) {
  if (request.aborted || (request.stream && request.stream.closed === true)) {
    return next()
  }
  const number = parseInt(error.status, 10)
  const valid = !isNaN(number) && number >= 400 && number < 600
  const code = valid ? number : 500
  const body = request.method === 'HEAD' ? ''
    : !valid ? 'Internal Server Error'
      : error.expose !== false ? error.message || 'Error'
        : 'Error'
  const headers = {
    'content-length': Buffer.byteLength(body),
    'content-type': 'text/plain; charset=utf-8'
  }
  if (!response.headersSent) {
    response.removeHeader('content-encoding')
    if (request.httpVersionMajor === 2) {
      response.writeHead(code, headers)
    } else {
      const message = statuses[code] || 'Error'
      response.writeHead(code, message, headers)
    }
  }
  if (!response.finished) {
    response.end(body)
  } else {
    response.end()
  }
  next()
}
